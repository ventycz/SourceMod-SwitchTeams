#pragma semicolon 1

#define DEBUG

#define PLUGIN_NAME "SwitchTeams"
#define PLUGIN_AUTHOR "VentyCZ"
#define PLUGIN_VERSION "0.01"

#include <sourcemod>
#include <cstrike>

// Use the new (1.7+ code style)
#pragma newdecls required

public Plugin myinfo = 
{
	name = PLUGIN_NAME, 
	author = PLUGIN_AUTHOR, 
	description = "Switches teams on command!", 
	version = PLUGIN_VERSION, 
	url = "https://hub.spawnpoint.cz/projects"
};

public void OnPluginStart()
{	
	// This plugin should be only used on CS:S server!
	if (GetEngineVersion() != Engine_CSS)
	{
		SetFailState("This plugin is for CSS only.");
	}
	
	PrintToServer("[SwitchTeams] Initializing...");	
	
	// Register team switch command
	RegServerCmd("switch_teams", SwitchTeamsCmd);
	
	// Round start/end event hooks
	HookEvent("round_start", onRoundStart);
	HookEvent("round_end", onRoundEnd);
}

int getPlayedRounds() {
	// Return combined team scores
	return CS_GetTeamScore(CS_TEAM_CT) + CS_GetTeamScore(CS_TEAM_T);
}

int getMaxRounds() {
	return FindConVar("mp_maxrounds").IntValue;
}

public Action onRoundStart(Handle event, char[] name, bool broadcast) {
	int round = getPlayedRounds() + 1; // +1 = current round
	int rounds = getMaxRounds();
	
	if (round > rounds) {
		return;
	}
	
	PrintToServer("Round %d is starting... (max: %d)", round, rounds);
	
	int halfRounds = rounds / 2;
	if (round == halfRounds) {
		PrintToChatAll("Last round of the first half, better make it count :)");
		PrintToServer("Last round of the first half!");
	}
}

public Action onRoundEnd(Handle event, char[] name, bool broadcast) {
	int round = getPlayedRounds();
	int rounds = getMaxRounds();
	
	if (round < 1) {
		return;
	}
	
	PrintToServer("Round %d ended! (max: %d)", round, rounds);
	
	int halfRounds = rounds / 2;
	if (round == halfRounds) {
		PrintToChatAll("Halftime - time to switch teams :)");
		PrintToServer("HALFTIME!");
	}
}

public Action SwitchTeamsCmd(int args) {
	SwitchTeams();
	
	return Plugin_Handled;
}

void SwitchTeams() {
	PrintToServer("[%s] Switching teams", PLUGIN_NAME);
	
	//Loop through all players and see if they are in game and that they are on a team
	for (int i = 1; i <= GetMaxClients(); i++)
	{
		// Skip invalid clients
		if (!IsClientInGame(i) || !IsClientConnected(i)) {
			continue;
		}
				
		int playerTeam = GetClientTeam(i);		
		if (playerTeam == CS_TEAM_T) {
			CS_SwitchTeam(i, CS_TEAM_CT);
		} else if (playerTeam == CS_TEAM_CT) {
			CS_SwitchTeam(i, CS_TEAM_T);
		}
	}
	
	// Get the team scores
	int ct_score = CS_GetTeamScore(CS_TEAM_CT);
	int t_score = CS_GetTeamScore(CS_TEAM_T);
	
	// Display the scores in the chat
	PrintToChatAll("Team scores before switch: T %d:%d CT", t_score, ct_score);
	
	// Restarts the game
	PrintToServer("[%s] Restarting game...", PLUGIN_NAME);
	ServerCommand("mp_restartgame 1");
	
	// Center text override - not really working
	PrintCenterTextAll("Switching teams...");
}