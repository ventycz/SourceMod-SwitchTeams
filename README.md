## SwitchTeams SourceMod plugin
Switches teams on command (`switch_teams`) and restarts the game (calls `mp_restartgame 1`).
### IDE Setup
- [SPedit](https://github.com/JulienKluge/Spedit)
